﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using SetupSQLite;
using SQLite;

namespace COMP6008_9961251_Assessment3
{
    public partial class MainPage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _connection.CreateTableAsync<movieInfo>();

        }
        async void addMovieButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new searchPage());
        }
        async void viewMovieButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new movieView());
        }
    }
}
