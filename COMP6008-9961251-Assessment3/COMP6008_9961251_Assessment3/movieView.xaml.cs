﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SetupSQLite;
using SQLite;

namespace COMP6008_9961251_Assessment3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class movieView : ContentPage
    {
        private static SQLiteAsyncConnection _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        private static List<movieInfo> info;
        public static int WImages = 100;
        public static int HImages = 175;
        public movieView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

        }
        async void getImages()
        {
            info = await _connection.Table<movieInfo>().ToListAsync();
            displayImages();

        }
        protected override void OnAppearing()
        {
            getImages();

            base.OnAppearing();
        }
        void displayImages()
        {
            displayGrid.VerticalOptions = LayoutOptions.FillAndExpand;
            displayGrid.RowSpacing = 0;
            displayGrid.ColumnSpacing = 0;
            var count = info.Count();
            var rowcount = count / 4;
            var z = 0;
            displayGrid.ColumnDefinitions = new ColumnDefinitionCollection
                    {
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
                    };
            displayGrid.RowDefinitions = new RowDefinitionCollection();
            while (z > rowcount)
            {
                displayGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                z++;
            }
            var i = 0;
            foreach (var x in info)
            {
                var img = new Image
                {
                    Source = x.image,
                    HeightRequest = HImages,
                    WidthRequest = WImages,
                };

                var indicator = new ActivityIndicator
                {
                    //IsRunning = true,
                    
                };
                indicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsLoading");
                indicator.BindingContext = img;

                var touchImage = new TapGestureRecognizer();
                touchImage.Tapped += Tapping;
                img.GestureRecognizers.Add(touchImage);

                var c = Convert.ToInt32(i % 4);
                var r = Convert.ToInt32(i / 4);

                Grid.SetColumn(img, c);
                Grid.SetRow(img, r);

                Grid.SetColumn(indicator, c);
                Grid.SetRow(indicator, r);

                displayGrid.Children.Add
                (
                    indicator
                );

                displayGrid.Children.Add
                (
                    img
                );
                i++;
            }
            scrollView.Orientation = ScrollOrientation.Vertical;
            scrollView.Content = displayGrid;
        }
        async void Tapping(object sender, EventArgs e)
        {
            var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * 4;
            var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);
            string dburl = "", dbtitle = "", dbyear = "", dbimageURL = "", dbdescription = "";
            int selectedID, dbid;
            selectedID = a + b; dbid = info[a + b].id; dburl = info[a + b].url; dbtitle = info[a + b].title; dbyear = info[a + b].year; dbimageURL = info[a + b].image; dbdescription = info[a + b].desc;
            var location = "movieView";
            await Navigation.PushAsync(new IMDBinfo(selectedID, dbid, dburl, dbtitle, dbyear, dbimageURL, dbdescription, location));
            this.Navigation.RemovePage(this);
        }
        async void GoBack(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
            this.Navigation.RemovePage(this);
        }
    }
}
