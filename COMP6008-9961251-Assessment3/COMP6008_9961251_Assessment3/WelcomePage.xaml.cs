﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMP6008_9961251_Assessment3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WelcomePage : ContentPage
	{
		public WelcomePage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            firstrun.Text = "FIRST RUN - WELCOME";
            scrollView.Orientation = ScrollOrientation.Vertical;
            scrollView.Content = steps;
        }
        async void getStartButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
            this.Navigation.RemovePage(this);
        }
    }
}
