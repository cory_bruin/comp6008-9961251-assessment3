﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net;
using System.Text.RegularExpressions;
using SetupSQLite;
using SQLite;

namespace COMP6008_9961251_Assessment3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class searchPage : ContentPage
    {
        public List<string> searchResults = new List<string>();
        public searchPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        async void searchButton(object sender, EventArgs e)
        {
            searchResults.Clear();
            searchGrid.Children.Clear();
            string desktopURL = "http://m.imdb.com/find?q=" + SearchEntry.Text.Replace(" ", "+");
            var client = new WebClient();
            using (var stream = client.OpenRead(desktopURL))
            using (var reader = new StreamReader(stream))
            {
                int itemsFound = 0;
                int itemsToFind = 4;
                string s;
                while ((s = reader.ReadLine()) != null)
                {
                    if (itemsToFind >= itemsFound)
                    {
                        if (s.Contains("<a href=\"/title/"))
                        {
                            MatchCollection urlLine = Regex.Matches(s, @"(?<=\bhref="")[^""]*");
                            var url = urlLine[0].ToString();
                            MatchCollection titleLine = Regex.Matches(s, @"<a(.*?)>(.*?)</a>");
                            var title = titleLine[0].Groups[2].ToString();
                            MatchCollection yearLine = Regex.Matches(s, @"\(([^)]*)\)", RegexOptions.RightToLeft);
                            var year = yearLine[0].ToString();
                            var touchLabel = new TapGestureRecognizer();
                            touchLabel.Tapped += Tapping;
                            var result = new Label
                            {
                                FontAttributes = FontAttributes.Bold,
                                FontSize = 16,
                                Text = $"{title} - {year}"
                            };
                            result.GestureRecognizers.Add(touchLabel);
                            searchResults.Add($"{url}");
                            searchGrid.Children.Add(result, 0, 1, itemsFound, (itemsFound + 1));
                            itemsFound++;
                        }
                    }
                }
            }
        }
        async void Tapping(object sender, EventArgs e)
        {
            string title = "", year = "", imageURL = "", description = "";
            int selectedID = 0, id = 0;
            var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty);
            var movieURL = "http://www.imdb.com" + searchResults[a];
            var client = new WebClient();
            using (var stream = client.OpenRead(movieURL))
            using (var reader = new StreamReader(stream))
            {
                var i = 0;
                int tFound = 0, iFound = 0, dFound = 0;
                int itemsToFind = 2;
                string s;
                while ((s = reader.ReadLine()) != null)
                {
                    i++;
                    if (s.Contains("og:title"))
                    {
                        tFound = 1;
                        MatchCollection titleLine = Regex.Matches(s, @"(?<=\bcontent="")[^""]*");
                        title = titleLine[0].ToString();
                        MatchCollection yearLine = Regex.Matches(title, @"\(([^)]*)\)", RegexOptions.RightToLeft);
                        title = title.Replace(yearLine[0].ToString(), "");
                        yearLine = Regex.Matches(yearLine[0].ToString(), @"\d+");
                        year = yearLine[0].ToString();
                    }
                    if (s.Contains("og:image"))
                    {
                        iFound = 1;
                        MatchCollection imageLine = Regex.Matches(s, @"(?<=\bcontent="")[^""]*");
                        imageURL = imageLine[0].ToString();
                        var image = new Image
                        {
                            Source = ImageSource.FromUri(new Uri(imageURL)),
                            Aspect = Aspect.AspectFit
                        };
                    }
                    if (s.Contains("og:description"))
                    {
                        dFound = 1;
                        MatchCollection descriptionLine = Regex.Matches(s, @"(?<=\bcontent="")[^""]*");
                        description = descriptionLine[0].ToString();
                    }
                    if ((tFound + iFound + dFound) > itemsToFind)
                    {
                        break;
                    }
                }
            }
            var location = "IMDBview";
            await Navigation.PushModalAsync(new IMDBinfo(selectedID, id, searchResults[a], title, year, imageURL, description, location));
        }
        async void GoBack(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }
    }
}
