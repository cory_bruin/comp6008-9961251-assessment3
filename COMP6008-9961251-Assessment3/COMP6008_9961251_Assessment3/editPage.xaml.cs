﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SetupSQLite;
using SQLite;
using System.Collections.ObjectModel;

namespace COMP6008_9961251_Assessment3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class editPage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<movieInfo> _url;
        public string dburl; string dbtitle; string dbyear; string dbimage; string dbdescription;
        public int selectedID, dbid;
        public editPage(int selected, int id, string url, string title, string year, string image, string description)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            dburl = url; dbtitle = title; dbyear = year; dbimage = image; dbdescription = description;
            TitleNew.Text = dbtitle;
            YearNew.Text = dbyear;
            ImageNew.Text = dbimage;
            DescNew.Text = dbdescription;
            selectedID = selected; dbid = id;

        }
        async void GoBack(object sender, EventArgs e)
        {
            dbtitle = TitleNew.Text;
            dbyear = YearNew.Text;
            dbimage = ImageNew.Text;
            dbdescription = DescNew.Text;
            var location = "movieView";
            await Navigation.PushAsync(new IMDBinfo(selectedID, dbid, dburl, dbtitle, dbyear, dbimage, dbdescription, location));
            Navigation.RemovePage(this);
        }
        async void updateMovie(object sender, EventArgs e)
        {
            var url = await _connection.Table<movieInfo>().ToListAsync();
            _url = new ObservableCollection<movieInfo>(url);
            await DisplayAlert("UPDATED", "Movie information updated", "OK");
            var urlBlock = _url[selectedID];
            urlBlock.title = TitleNew.Text;
            urlBlock.year = YearNew.Text;
            urlBlock.image = ImageNew.Text;
            urlBlock.desc = DescNew.Text;

            await _connection.UpdateAsync(urlBlock);
        }
    }
}
