﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMP6008_9961251_Assessment3
{
    public class movieInfo
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int id { get; set; }

        public string url { get; set; }
        public string image { get; set; }
        public string title { get; set; }
        public string year { get; set; }
        public string desc { get; set; }
    }
}
