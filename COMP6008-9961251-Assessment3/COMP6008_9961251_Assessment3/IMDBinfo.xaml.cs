﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net;
using System.Text.RegularExpressions;
using SetupSQLite;
using SQLite;
using System.Collections.ObjectModel;

namespace COMP6008_9961251_Assessment3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IMDBinfo : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<movieInfo> _url;
        public string dburl = "", dbtitle = "", dbyear = "", dbimageURL = "", dbdescription = "", fromLocation = "";
        public int selectedID, dbid;
        public IMDBinfo(int selected, int id, string url, string title, string year, string imageURL, string description, string location)
        {
            var pageFrom = location;
            selectedID = selected; dbid = id; dburl = url; dbtitle = title; dbyear = year; dbimageURL = imageURL; dbdescription = description; fromLocation = location;
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            detailGrid.Children.Add(new Label { HorizontalOptions = LayoutOptions.CenterAndExpand, FontAttributes = FontAttributes.Bold, FontSize = 16, Text = $"{dbtitle} - ({dbyear})" }, 0, 3, 0, 1);
            var image = new Image
            {
                Source = ImageSource.FromUri(new Uri(dbimageURL)),
                Aspect = Aspect.AspectFit
            };
            detailGrid.Children.Add(image, 1, 1);
            detailGrid.Children.Add(new Label { FontSize = 14, Text = $"Description: {dbdescription}" }, 0, 3, 2, 3);
            if (pageFrom == "IMDBview")
            {
                Button saveButton;
                detailGrid.Children.Add(saveButton = new Button { BackgroundColor = Color.Default, FontSize = 15, Text = $"Confirm Save" }, 1, 2, 3, 4);
                saveButton.Clicked += saveButton_Clicked;
            }
            if (pageFrom == "movieView")
            {
                Button editButton;
                detailGrid.Children.Add(editButton = new Button { BackgroundColor = Color.Default, FontSize = 15, Text = $"Edit Movie" }, 1, 2, 3, 4);
                editButton.Clicked += editButton_Clicked;
            }
            if (pageFrom == "movieView")
            {
                Button deleteButton;
                detailGrid.Children.Add(deleteButton = new Button { BackgroundColor = Color.Default, FontSize = 15, Text = $"Delete Movie" }, 1, 2, 4, 5);
                deleteButton.Clicked += deleteButton_Clicked;
            }
            scrollView.Orientation = ScrollOrientation.Vertical;
            scrollView.Content = detailGrid;
        }

        async void saveButton_Clicked(object sender, EventArgs e)
        {
            var movieInfo = new movieInfo { url = dburl, image = dbimageURL, title = dbtitle, year = dbyear, desc = dbdescription };
            await _connection.InsertAsync(movieInfo);
            await DisplayAlert("ADDED", "Movie Info added successfully", "OK");
        }
        async void editButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new editPage(selectedID, dbid, dburl, dbtitle, dbyear, dbimageURL, dbdescription));
            this.Navigation.RemovePage(this);
        }
        async void deleteButton_Clicked(object sender, EventArgs e)
        {
            var url = await _connection.Table<movieInfo>().ToListAsync();
            _url = new ObservableCollection<movieInfo>(url);
            var urlBlock = _url[selectedID];
            await _connection.DeleteAsync(urlBlock);
            var proceed = await DisplayAlert("DELETED", "MOVIE DELETED", null, "OK");
            if (!proceed)
            {
                await Navigation.PushAsync(new movieView());
            }
        }
        async void GoBack(object sender, EventArgs e)
        {
            if (fromLocation == "movieView")
            {
                await Navigation.PushAsync(new movieView());
                this.Navigation.RemovePage(this);
            }
            if (fromLocation == "IMDBview")
            {
                await Navigation.PopModalAsync();
            }
        }
    }
}
