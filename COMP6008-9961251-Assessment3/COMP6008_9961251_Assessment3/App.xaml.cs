﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace COMP6008_9961251_Assessment3
{

	public partial class App : Application
	{
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());

        }

        protected override void OnStart ()
		{
            // Handle when your app starts
            if (!Application.Current.Properties.ContainsKey("startUp"))
            {
                Application.Current.Properties["startUp"] = 1;
                MainPage = new NavigationPage(new WelcomePage());
            }
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
